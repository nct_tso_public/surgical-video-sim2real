import open3d as o3d
import torch
import cv2

from viz_util import make_cam_frame, make_unit_frame

H = 256
W = 512

path_img = '../data/simulated/ExampleScene_FakeLiver_0/im0000.png'
path_renderdata = '../data/simulated/ExampleScene_FakeLiver_0/corrData0000.tar'

geometries = []

rgb = cv2.imread(path_img)
rgb = torch.Tensor(cv2.cvtColor(rgb, cv2.COLOR_BGR2RGB)) / 255

frame_data = torch.load(path_renderdata)
cam_pose = torch.Tensor(frame_data['cam_pose']) # shape 7 (xyz + quaterions)
depth = frame_data['depth'] # shape HxW
xyz = frame_data['points3D'] # shape (W*H)x4 in homogeneous coordinates
xyz = xyz[:,:3].view(W,H,3).transpose(1,0) # convert from shape (W*H)x4 to HxWx3

geometries.append(make_cam_frame(cam_pose,xyz))

scene_pcd = o3d.geometry.PointCloud(
	points = o3d.utility.Vector3dVector(xyz.reshape(-1,3)),
)
scene_pcd.colors = o3d.utility.Vector3dVector(rgb.reshape(-1,3))
geometries.append(scene_pcd)

o3d.visualization.draw_geometries(geometries)